//
//  StringToNumberHelper.m
//  testTaskFor2gis
//
//  Created by Владислав on 09.08.16.
//  Copyright © 2016 vladislav. All rights reserved.
//
#import "StringToNumberHelper.h"

@interface StringToNumberHelper() {
    
}

@end


@implementation StringToNumberHelper

+ (NSString*)pushNewNumber:(NSString*)number newNumber:(NSString*)newNumber {
    
    if([number isEqualToString:@"0"]) {
        
        if([newNumber isEqualToString:@"."]) {
            number = [number stringByAppendingString:newNumber];
        } else {
            number = newNumber;
        }
    } else {
        
        if([newNumber isEqualToString:@"."]) {
            
            if((![number containsString:@"."])&&(![number isEqualToString:@"0"])) {
                number = [number stringByAppendingString:newNumber];
            }
            
        } else {
            
            if([number containsString:@"."]) {
                
                if([StringToNumberHelper numberOfDigitsAfterPointInString:number] < 2) {
                    number = [number stringByAppendingString:newNumber];
                }
            } else {
                number = [number stringByAppendingString:newNumber];
            }
        }
    }
    return number;
}

+ (NSString*)delLastNumber:(NSString*)number  {
    
    if([number isEqualToString:@"0"]) {
        return number;
    } else if([number length] == 1 ) {
        return @"0";
    } else {
        return [number substringToIndex:[number length] - 1];
    }
}

+ (int)numberOfDigitsAfterPoint:(float)number {
    
    NSString *string = [[NSNumber numberWithFloat:number] stringValue];
    
    return [StringToNumberHelper numberOfDigitsAfterPointInString:string];
}

+ (int)numberOfDigitsAfterPointInString:(NSString*)string {
    
    NSRange range = [string rangeOfString:@"."];
    int digits;
    if (range.location != NSNotFound) {
        string = [string substringFromIndex:range.location + 1];
        digits = (int)[string length];
    } else {
        range = [string rangeOfString:@"e-"];
        if (range.location != NSNotFound) {
            string = [string substringFromIndex:range.location + 2];
            digits = [string intValue];
        } else {
            digits = 0;
        }
    }
    return digits;
}

+ (NSString*)formatForNumberOutput:(int)numberOfDigitsAfterPoint {
    
    if(numberOfDigitsAfterPoint == 0) {
        return @"%1.0f";
    } else if(numberOfDigitsAfterPoint == 1) {
        return @"%1.1f";
    } else {
        return @"%1.2f";
    }
}

@end
