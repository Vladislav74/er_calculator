//
//  StringToNumberHelper.h
//  testTaskFor2gis
//
//  Created by Владислав on 09.08.16.
//  Copyright © 2016 vladislav. All rights reserved.
//
#import <Foundation/Foundation.h>

@interface StringToNumberHelper : NSObject

+ (NSString*)pushNewNumber:(NSString*)number newNumber:(NSString*)newNumber;
+ (NSString*)delLastNumber:(NSString*)number;

+ (int)numberOfDigitsAfterPoint:(float)number;
+ (int)numberOfDigitsAfterPointInString:(NSString*)string;
+ (NSString*)formatForNumberOutput:(int)numberOfDigitsAfterPoint;

@end
