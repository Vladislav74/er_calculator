//
//  ValutaCell.h
//  testTaskFor2gis
//
//  Created by Владислав on 09.08.16.
//  Copyright © 2016 vladislav. All rights reserved.
//
#import <UIKit/UIKit.h>
#import "Valuta.h"


@interface ValutaCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *valutaNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *valutaValueLabel;
@property (weak, nonatomic) Valuta* valutaObj;

@end
