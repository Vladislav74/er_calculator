//
//  SettingsViewController
//  testTaskFor2gis
//
//  Created by Владислав on 09.08.16.
//  Copyright © 2016 vladislav. All rights reserved.
//
#import "SettingsViewController.h"
#import "UserSettings.h"

@interface SettingsViewController() <UITableViewDelegate, UITableViewDataSource> {
    UIColor *backgroundColor;
}

@end

@implementation SettingsViewController

- (IBAction)doneButtonAction:(UIBarButtonItem *)sender {
    
    [[UserSettings instance] saveSettings];
    
    [self dismissViewControllerAnimated:YES completion:nil];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    backgroundColor = [[UIColor alloc] initWithRed:61.0/255 green:135.0/255 blue:221.0/255 alpha:1];
    
    self.tableView.backgroundColor = backgroundColor;
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(section == 0) {
        return 1;
    } else {
        return [[[UserSettings instance] listOfPossibleValutesWithoutMainValuta] count];
    }
}

- (nullable NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if(section == 0) {
        return @"Валюта вашей страны";
    } else {
        return @"Список валют";
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellIdentifier = @"ValutaSettingCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    if(indexPath.section == 0) {
        cell.textLabel.text = [[UserSettings instance] mainValutaName];
        cell.accessoryType = UITableViewCellAccessoryNone;
    } else {
        
        NSString *valutaName = [[[UserSettings instance] listOfPossibleValutesWithoutMainValuta] objectAtIndex:indexPath.row];
        cell.textLabel.text = valutaName;
        
        NSDictionary *userSettings = [[UserSettings instance] settings];
        NSNumber *settingForValuta = [userSettings valueForKey:valutaName];
        BOOL flag = settingForValuta.boolValue;
        
        if(flag) {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        } else {
            cell.accessoryType = UITableViewCellAccessoryNone;
        }
    }
    cell.backgroundColor = backgroundColor;
    cell.tintColor = [UIColor blackColor];
    
    return cell;
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if(indexPath.section != 0) {
        
        NSString *valutaName = [self.tableView cellForRowAtIndexPath:indexPath].textLabel.text;
        NSDictionary *userSettings = [[UserSettings instance] settings];
        
        BOOL flag;
        if([self.tableView cellForRowAtIndexPath:indexPath].accessoryType == UITableViewCellAccessoryNone) {
            flag = YES;
            [self.tableView cellForRowAtIndexPath:indexPath].accessoryType = UITableViewCellAccessoryCheckmark;
        } else {
            flag = NO;
            [self.tableView cellForRowAtIndexPath:indexPath].accessoryType = UITableViewCellAccessoryNone;
        }
        
        NSNumber *newSettingForValuta = [[NSNumber alloc] initWithBool:flag];
        [userSettings setValue:newSettingForValuta forKey:valutaName];
    }
}

@end