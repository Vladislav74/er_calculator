//
//  CalculatorViewController.m
//  testTaskFor2gis
//
//  Created by Владислав on 09.08.16.
//  Copyright © 2016 vladislav. All rights reserved.
//
#import "CalculatorViewController.h"
#import "ExchangeRateCalculatorEngine.h"
#import "StringToNumberHelper.h"
#import "Valuta.h"
#import "ValutaCell.h"

@interface CalculatorViewController() <UITableViewDelegate, UITableViewDataSource> {
    ValutaCell* celectedCell;
    
    NSString *number;
    NSUInteger celectedIndex;
    
    UIColor *backgroundColor;
}

@property (weak, nonatomic) IBOutlet UINavigationBar *navigationBar;
@property (weak, nonatomic) IBOutlet UITableView *valutaCostTableView;

@end

@implementation CalculatorViewController

- (IBAction)refreshButtonAction:(UIBarButtonItem *)sender {
    [[ExchangeRateCalculatorEngine instance] reloadData];
}

- (IBAction)keyboardButtonAction:(UIButton *)sender {
    
    NSAssert(([sender.titleLabel.text isEqualToString:@"<-"] ||
              [sender.titleLabel.text isEqualToString:@"."] ||
              [sender.titleLabel.text isEqualToString:@"0"] ||
              [sender.titleLabel.text isEqualToString:@"1"] ||
              [sender.titleLabel.text isEqualToString:@"2"] ||
              [sender.titleLabel.text isEqualToString:@"3"] ||
              [sender.titleLabel.text isEqualToString:@"4"] ||
              [sender.titleLabel.text isEqualToString:@"5"] ||
              [sender.titleLabel.text isEqualToString:@"6"] ||
              [sender.titleLabel.text isEqualToString:@"7"] ||
              [sender.titleLabel.text isEqualToString:@"8"] ||
              [sender.titleLabel.text isEqualToString:@"9"] ), @"button for number contains wrong text label");
    
    if(celectedCell == nil) {
        
        NSIndexPath *indexPathForFirstRow = [NSIndexPath indexPathForRow:0 inSection:0];
        [self.valutaCostTableView selectRowAtIndexPath:indexPathForFirstRow animated:YES scrollPosition:UITableViewScrollPositionNone];
        celectedCell = [self.valutaCostTableView cellForRowAtIndexPath:indexPathForFirstRow];
    }
    
    if(celectedCell != nil) {
        
        Valuta *valutaObj = celectedCell.valutaObj;
        
        if([sender.titleLabel.text isEqualToString:@"<-"]) {
            
            number = [StringToNumberHelper delLastNumber:number];
            
            [valutaObj newValue:[number floatValue]];
            
            [self.valutaCostTableView reloadData];
            
        } else {
            
            number = [StringToNumberHelper pushNewNumber:number newNumber:sender.titleLabel.text];
            
            [valutaObj newValue:[number floatValue]];
            
            [self.valutaCostTableView reloadData];
        }
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    backgroundColor = [[UIColor alloc] initWithRed:61.0/255 green:135.0/255 blue:221.0/255 alpha:1];
    self.view.backgroundColor = backgroundColor;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(newDataWasObtained:)
                                                 name:notificationDataWasObtained
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(newDataWasNotObtained:)
                                                 name:notificationDataWasNotObtained
                                               object:nil];
    
    self.valutaCostTableView.dataSource = self;
    self.valutaCostTableView.delegate = self;
    self.valutaCostTableView.backgroundColor = backgroundColor;
    self.navigationBar.backgroundColor = backgroundColor;
    
    number = @"1";
    celectedIndex = 0;
    
    [ExchangeRateCalculatorEngine instance];
}

#pragma mark - notifications
- (void)newDataWasObtained:(NSNotification*)notification {
    
    celectedIndex = 0;
    number = @"1";
    
    [self.valutaCostTableView reloadData];
}

- (void)newDataWasNotObtained:(NSNotification*)notification {
    
    UIAlertController *alert = [UIAlertController
                                alertControllerWithTitle:@"Oops!"
                                message:@"Oops we can't obtain data from server"
                                preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *ok = [UIAlertAction
                         actionWithTitle:@"OK"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             [alert dismissViewControllerAnimated:YES completion:nil];
                         }];
    
    [alert addAction:ok];
    
    [self presentViewController:alert animated:YES completion:nil];
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [[ExchangeRateCalculatorEngine instance] numberOfValutes];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath {
    
    static NSString* cellIdentifier = @"ValutaCell";
    
    ValutaCell *cell = (ValutaCell*)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:cellIdentifier owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    Valuta *valuta = [[ExchangeRateCalculatorEngine instance] valutaAtIndex:indexPath.row];
    
    cell.valutaNameLabel.text = [valuta name];
    cell.backgroundColor = backgroundColor;
    
    if(celectedIndex == indexPath.row) {
        cell.valutaValueLabel.text = number;
    } else {
        NSString *stringFormat = [StringToNumberHelper formatForNumberOutput:[StringToNumberHelper numberOfDigitsAfterPoint:[valuta value]]];
        cell.valutaValueLabel.text = [NSString stringWithFormat:stringFormat, [valuta value]];
    }
    cell.valutaObj = valuta;
    
    return cell;
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    number = @"0";
    
    NSIndexPath *indexPathForFirstRow = [NSIndexPath indexPathForRow:0 inSection:0];
    ValutaCell *firstCell = [self.valutaCostTableView cellForRowAtIndexPath:indexPathForFirstRow];
    [firstCell.valutaObj newValue:0];
    
    celectedIndex = indexPath.row;
    celectedCell = [self.valutaCostTableView cellForRowAtIndexPath:indexPath];
    
    [self.valutaCostTableView reloadData];
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if(celectedIndex == indexPath.row){
        NSIndexPath *indexPathForSelectedRow = [NSIndexPath indexPathForRow:celectedIndex inSection:0];
        [self.valutaCostTableView selectRowAtIndexPath:indexPathForSelectedRow animated:NO scrollPosition:UITableViewScrollPositionNone];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void) dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
