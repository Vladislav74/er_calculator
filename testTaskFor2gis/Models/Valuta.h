//
//  Valuta.h
//  testTaskFor2gis
//
//  Created by Владислав on 09.08.16.
//  Copyright © 2016 vladislav. All rights reserved.
//
#import <Foundation/Foundation.h>

@interface Valuta : NSObject

- (id)initWith:(NSString*)newName relationToTheBase:(float)newRelationToTheBase pointerToBase:(Valuta*)base ;

- (NSString*)name;
- (NSString*)valueAsString;
- (float)value;
- (void)newValue:(float)newValue;

@end
