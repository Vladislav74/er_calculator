//
//  ExchangeRateDataObtainer.m
//  testTaskFor2gis
//
//  Created by Владислав on 09.08.16.
//  Copyright © 2016 vladislav. All rights reserved.
//
#import "ExchangeRateDataObtainer.h"

@interface ExchangeRateDataObtainer() {
    
}

- (void)fetchJsonDataFromUrl:(NSString*)url Completetion:(void (^) (NSDictionary *result,NSError *error))completion;

@end

@implementation ExchangeRateDataObtainer

static NSString *urlBase = @"http://api.fixer.io/latest?base=";

- (void)fetchExchangeRateDataWhereLocalCalutaIs:(NSString*)valutaName Completetion:(void (^) (NSDictionary *result, NSString *baseValuta, NSError *error))completion {
    
    NSString *urlAsString = [urlBase stringByAppendingString:valutaName];
    
    [self fetchJsonDataFromUrl:urlAsString Completetion:^(NSDictionary *result, NSError *error) {
        
        NSDictionary *dataDictionary = [NSDictionary alloc];
        NSString *baseValuta = [NSString alloc];
        
        if(error == nil) {
            dataDictionary = [result objectForKey: @"rates"];
            baseValuta = [result objectForKey:@"base"];
        } else {
            dataDictionary = nil;
            baseValuta = nil;
        }
        
        completion(dataDictionary, baseValuta, error);
    }];
}

//helper
- (void)fetchJsonDataFromUrl:(NSString*)url Completetion:(void (^) (NSDictionary* result,NSError* error))completion {
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setHTTPMethod:@"GET"];
    [request setURL:[NSURL URLWithString:url]];
    
    NSURLSessionDataTask *dataTask = [[NSURLSession sharedSession]
                                      dataTaskWithRequest:request
                                      completionHandler:^(NSData* _Nullable data, NSURLResponse* _Nullable response, NSError* _Nullable error) {
                                          
                                          if(data == nil) {
                                              completion(nil,error);
                                              return;
                                          }
                                          
                                          NSError *myError;
                                          NSDictionary *dataDictionary = [NSJSONSerialization JSONObjectWithData:data
                                                                                                         options:kNilOptions
                                                                                                           error:&myError];
                                          completion(dataDictionary,myError);
                                      }];
    [dataTask resume];
}

@end
