//
//  Valuta.m
//  testTaskFor2gis
//
//  Created by Владислав on 09.08.16.
//  Copyright © 2016 vladislav. All rights reserved.
//
#import "Valuta.h"

@interface Valuta(){
    
    Valuta *pointerToBase;
    
    NSString *name;
    NSString *valueAsString;
    float relationToTheBase;
    float value;
}

@end

@implementation Valuta

- (id)initWith:(NSString*)newName relationToTheBase:(float)newRelationToTheBase pointerToBase:(Valuta*)base {
    self = [super init];
    
    pointerToBase = base;
    
    name = newName;
    relationToTheBase = newRelationToTheBase;
    value = 0.0;
    
    return self;
}

- (NSString*)name {
    return name;
}

- (NSString*)valueAsString {
    return valueAsString;
}

- (float)value {
    if(pointerToBase != nil) {
        return ([pointerToBase value] * relationToTheBase);
    } else {
        return value;
    }
}

- (void)newValue:(float)newValue {
    if(pointerToBase != nil) {
        [pointerToBase newValue:(newValue/relationToTheBase)];
    } else {
        value = newValue;
    }
}

@end
