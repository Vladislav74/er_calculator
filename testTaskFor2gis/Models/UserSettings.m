//
//  UserSettings.m
//  testTaskFor2gis
//
//  Created by Владислав on 09.08.16.
//  Copyright © 2016 vladislav. All rights reserved.
//
#import "UserSettings.h"

/// notifications
NSString* const notificationUserSettingsWasUpdated = @"userSettingsWasUpdated";
NSString* const userDefaultsKeyForUserSettings = @"settings";


@interface UserSettings() {
    
    NSString *mainValutaName;
    NSMutableDictionary *listOfUserChosenValutes;
    NSArray *userChosenValutesArray;
    
    NSArray *listOfValutes;
    NSArray *listOfAllPossibleValutes;
}

@end

@implementation UserSettings

+ (instancetype)instance {
    static UserSettings *instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[[self class] alloc] initPrivate];
    });
    return instance;
}

- (instancetype)init {
    NSAssert(0, @"Use 'instance' instead");
    return nil;
}

- (instancetype)initPrivate {
    if (self = [super init]) {
        
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        
        NSDictionary *settings = [userDefaults dictionaryForKey:userDefaultsKeyForUserSettings];
        
        mainValutaName = [settings valueForKey:@"userMainValuta"];
        listOfUserChosenValutes = [[settings valueForKey:@"userSettings"] mutableCopy];
        
        (void)[self userChosenValutes];
        
        listOfAllPossibleValutes = [[NSArray alloc] initWithObjects:@"RUB",
                                                                    @"AUD",
                                                                    @"BGN",
                                                                    @"BRL",
                                                                    @"CAD",
                                                                    @"CHF",
                                                                    @"CNY",
                                                                    @"CZK",
                                                                    @"DKK",
                                                                    @"GBP",
                                                                    @"HRK",
                                                                    @"HUF",
                                                                    @"IDR",
                                                                    @"ILS",
                                                                    @"INR",
                                                                    @"JPY",
                                                                    @"KRW",
                                                                    @"MXN",
                                                                    @"MYR",
                                                                    @"NOK",
                                                                    @"NZD",
                                                                    @"PHP",
                                                                    @"PLN",
                                                                    @"RON",
                                                                    @"SEK",
                                                                    @"SGD",
                                                                    @"THB",
                                                                    @"TRY",
                                                                    @"USD",
                                                                    @"ZAR",
                                                                    @"EUR", nil];
        
    }
    return self;
}

- (NSArray*)listOfPossibleValutes {
    return listOfAllPossibleValutes;
}

- (NSMutableDictionary*)settings {
    return listOfUserChosenValutes;
}

- (void)saveSettings {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    NSDictionary *settings = @{ @"userMainValuta" : mainValutaName,
                                @"userSettings" : listOfUserChosenValutes};
    
    [userDefaults setObject:settings forKey:userDefaultsKeyForUserSettings];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:notificationUserSettingsWasUpdated
                                                        object:nil];
}

- (void)newNameForMainValuta:(NSString*)newName {
    mainValutaName = newName;
    [self saveSettings];
}

- (NSString*)mainValutaName {
    return mainValutaName;
}

- (NSArray*)listOfPossibleValutesWithoutMainValuta {
    
    NSMutableArray *tempArray = [[NSMutableArray alloc] init];
    
    for(NSString *valuta in listOfAllPossibleValutes) {
        if(![valuta isEqualToString:mainValutaName]) {
            [tempArray addObject:valuta];
        }
    }
    NSArray *listOfPossibleValutesWithoutMainValuta = [NSArray arrayWithArray:tempArray];
    return listOfPossibleValutesWithoutMainValuta;
}

- (NSArray*)userChosenValutes {
    NSMutableArray *tempArray = [[NSMutableArray alloc] init];
    
    for(NSString *key in listOfAllPossibleValutes){
        
        NSNumber *settingForValuta = [listOfUserChosenValutes valueForKey:key];
        
        if(settingForValuta.boolValue) {
            if(![key isEqualToString:mainValutaName]) {
                [tempArray addObject:key];
            }
        }
    }
    userChosenValutesArray = [NSArray arrayWithArray:tempArray];
    return userChosenValutesArray;
}

+ (void)setStartSettings {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    NSDictionary *userSettings = @{@"RUB" : [NSNumber numberWithBool:YES],
                                   @"AUD" : [NSNumber numberWithBool:YES],
                                   @"BGN" : [NSNumber numberWithBool:YES],
                                   @"BRL" : [NSNumber numberWithBool:YES],
                                   @"CAD" : [NSNumber numberWithBool:YES],
                                   @"CHF" : [NSNumber numberWithBool:YES],
                                   @"CNY" : [NSNumber numberWithBool:YES],
                                   @"CZK" : [NSNumber numberWithBool:YES],
                                   @"DKK" : [NSNumber numberWithBool:YES],
                                   @"GBP" : [NSNumber numberWithBool:YES],
                                   @"HRK" : [NSNumber numberWithBool:YES],
                                   @"HUF" : [NSNumber numberWithBool:YES],
                                   @"IDR" : [NSNumber numberWithBool:YES],
                                   @"ILS" : [NSNumber numberWithBool:YES],
                                   @"INR" : [NSNumber numberWithBool:YES],
                                   @"JPY" : [NSNumber numberWithBool:YES],
                                   @"KRW" : [NSNumber numberWithBool:YES],
                                   @"MXN" : [NSNumber numberWithBool:YES],
                                   @"MYR" : [NSNumber numberWithBool:YES],
                                   @"NOK" : [NSNumber numberWithBool:YES],
                                   @"NZD" : [NSNumber numberWithBool:YES],
                                   @"PHP" : [NSNumber numberWithBool:YES],
                                   @"PLN" : [NSNumber numberWithBool:YES],
                                   @"RON" : [NSNumber numberWithBool:YES],
                                   @"SEK" : [NSNumber numberWithBool:YES],
                                   @"SGD" : [NSNumber numberWithBool:YES],
                                   @"THB" : [NSNumber numberWithBool:YES],
                                   @"TRY" : [NSNumber numberWithBool:YES],
                                   @"USD" : [NSNumber numberWithBool:YES],
                                   @"ZAR" : [NSNumber numberWithBool:YES],
                                   @"EUR" : [NSNumber numberWithBool:YES], };
    
    NSString *userMainValuta = @"RUB";
    
    NSDictionary *settings = @{ @"userMainValuta" : userMainValuta,
                                @"userSettings" : userSettings};
    
    [userDefaults registerDefaults:@{ userDefaultsKeyForUserSettings : settings }];
}

@end