//
//  ExchangeRateDataObtainer.h
//  testTaskFor2gis
//
//  Created by Владислав on 09.08.16.
//  Copyright © 2016 vladislav. All rights reserved.
//
#import <Foundation/Foundation.h>

@interface ExchangeRateDataObtainer : NSObject

- (void)fetchExchangeRateDataWhereLocalCalutaIs:(NSString*)valutaName Completetion:(void (^) (NSDictionary* result, NSString* baseValuta, NSError* error))completion;

@end
