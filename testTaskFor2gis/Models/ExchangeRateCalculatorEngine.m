//
//  ExchangeRateCalculatorEngine.m
//  testTaskFor2gis
//
//  Created by Владислав on 09.08.16.
//  Copyright © 2016 vladislav. All rights reserved.
//
#import "ExchangeRateCalculatorEngine.h"
#import "ExchangeRateDataObtainer.h"
#import "UserSettings.h"


/// notifications
NSString* const notificationDataWasObtained = @"newDataWasObtained";
NSString* const notificationDataWasNotObtained = @"newDataWasNotObtained";

@interface ExchangeRateCalculatorEngine() {
    
    ExchangeRateDataObtainer* dataObtainer;
    
    NSMutableArray* arrayOfValutes;
}

@end

@implementation ExchangeRateCalculatorEngine

+ (instancetype)instance {
    static ExchangeRateCalculatorEngine *instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[[self class] alloc] initPrivate];
    });
    return instance;
}

- (instancetype)init {
    NSAssert(0, @"Use 'instance' instead");
    return nil;
}

- (instancetype)initPrivate {
    if (self = [super init]) {
        dataObtainer = [[ExchangeRateDataObtainer alloc] init];
        
        arrayOfValutes = [[NSMutableArray alloc] init];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(userSettingsWasUpdated:)
                                                     name:notificationUserSettingsWasUpdated
                                                   object:nil];
        
        [self reloadData];
        
    }
    return self;
}

- (void)reloadData {
    
    [arrayOfValutes removeAllObjects];
    
    UserSettings *us = [UserSettings instance];
    
    [dataObtainer fetchExchangeRateDataWhereLocalCalutaIs:[us mainValutaName]
                                             Completetion:^(NSDictionary *result, NSString *baseValuta, NSError *error) {
                                                 
                                                 NSString *notification = notificationDataWasObtained;
                                                 
                                                 if(error == nil) {
                                                     
                                                     NSString* mainValutaName = [[UserSettings instance] mainValutaName];
                                                     
                                                     Valuta *mainValuta = [[Valuta alloc] initWith:mainValutaName relationToTheBase:1 pointerToBase:nil];
                                                     [mainValuta newValue:1.0];
                                                     
                                                     [arrayOfValutes addObject:mainValuta];
                                                     
                                                     NSArray *userChosenValutes = [us userChosenValutes];
                                                     
                                                     for (NSString *vName in userChosenValutes) {
                                                         
                                                         float relation = [[result objectForKey:vName] floatValue];
                                                         
                                                         Valuta *vluta = [[Valuta alloc] initWith: vName relationToTheBase: relation pointerToBase:mainValuta];
                                                         
                                                         [arrayOfValutes addObject:vluta];
                                                     }
                                                 } else {
                                                     notification = notificationDataWasNotObtained;
                                                 }
                                                 
                                                 dispatch_async(dispatch_get_main_queue(), ^{
                                                     [[NSNotificationCenter defaultCenter] postNotificationName:notification
                                                                                                         object:nil];
                                                 });
                                                 
                                             }];
}

- (NSInteger)numberOfValutes {
    return [arrayOfValutes count];
}

- (Valuta*)valutaAtIndex:(NSUInteger)index {
    return (Valuta*)[arrayOfValutes objectAtIndex:index];
}

#pragma mark - notifications
- (void)userSettingsWasUpdated:(NSNotification*)notification {
    
    [self reloadData];
}


- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
