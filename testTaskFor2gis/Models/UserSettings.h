//
//  UserSettings.h
//  testTaskFor2gis
//
//  Created by Владислав on 09.08.16.
//  Copyright © 2016 vladislav. All rights reserved.
//
#import <Foundation/Foundation.h>

/// notifications
FOUNDATION_EXPORT NSString* const notificationUserSettingsWasUpdated;

@interface UserSettings : NSObject

+ (instancetype)instance;

- (NSArray*)listOfPossibleValutes;
- (NSMutableDictionary*)settings;
- (void)saveSettings;
- (void)newNameForMainValuta:(NSString*)newName;
- (NSString*)mainValutaName;
- (NSArray*)listOfPossibleValutesWithoutMainValuta;
- (NSArray*)userChosenValutes;

+ (void)setStartSettings;

@end
