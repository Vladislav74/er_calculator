//
//  ExchangeRateCalculatorEngine.h
//  testTaskFor2gis
//
//  Created by Владислав on 09.08.16.
//  Copyright © 2016 vladislav. All rights reserved.
//
#import <Foundation/Foundation.h>
#import "Valuta.h"

/// notifications
FOUNDATION_EXPORT NSString* const notificationDataWasObtained;
FOUNDATION_EXPORT NSString* const notificationDataWasNotObtained;

@interface ExchangeRateCalculatorEngine : NSObject

+ (instancetype)instance;
- (void)reloadData;

- (NSInteger)numberOfValutes;
- (Valuta*)valutaAtIndex:(NSUInteger)index;

@end
